<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=vagrant_yii2_template',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',
];
